\documentclass[../l4proj.tex]{subfiles}

\begin{document}
	\chapter{Introduction}

	%Why should the reader care about what are you doing and what are you actually doing?

%	 Problem formulation, what are the requirements, FPGA communication with PC, step by step, 
% what does it contribute to

% emphasis on trying to build a computer, not necessary Sigma16

	\begin{quote}
		``Given a box of logic gates, can we build a computer?''
	\end{quote}
	Many computer scientists know how high level programming language instructions usually translate to multiple basic operations and these operations get translated to machine code which is then executed by the computer. However the actual execution process is a mystery to many of them. How can we achieve such complex operations starting with very simple logic gates?
	
    To grasp the core ideas how this is done, we do not need to build something even close to modern industrial computers. We can use some much simpler architectures. There are three semesters taught at University of Glasgow using one such architecture -- Sigma16. Developed by Honorary Lecturer Dr John O'Donnell. The architecture however is either simulated or emulated, it has never been implemented on a real hardware circuit.
    
    Our aim is to demonstrate the foundations of how does a computer operate on hardware (logic gates and signals) level using the Sigma16 computer architecture.
	
	\section{Glossary}
	
	\begin{description}[leftmargin=!,labelwidth=\widthof{\bfseries Quartus II}]
		\item[FPGA] Field Programmable Gate Array (section \ref{sec:FPGA}).
		\item[Hydra] A functional computer hardware description language (section \ref{sec:Hydra}).
		\item[Quartus II] A software tool for designing programmable logic devices, produced by Altera.
		\item[M1] Circuit implementation of Sigma16 architecture written in Hydra (section \ref{sec:Sigma16}).
		\item[Sigma16] A computer architecture designed for teaching purposes (section \ref{sec:Sigma16}).
		\item[VHDL] (Very High Speed Integrated Circuit) Hardware Description Language (section \ref{sec:VHDL}).
	\end{description}
	
	\section{Basic Logic Circuit Components}\label{sec:bool}
	Let's do a quick recapitulation of basic boolean algebra and logic units. Table \ref{tab:bool} contains overview of the most basic operations (\texttt{not}, \texttt{and}, \texttt{or} and \texttt{xor}). From these we will build up more complex circuits.
	
	\begin{table}[h]
		\caption{Truth table of basic logic gates, including symbols and notation for the operations} \label{tab:bool}
		\centering
		\rowcolors{2}{}{gray!3}
		\begin{tabular}{c c|c c c c}
			\multicolumn{2}{c|}{\textbf{Symbol}} &
				\begin{tikzpicture}
					\node[not gate US, draw] at (0, 0) (gate) {};
					\draw ($ (gate.input) -  (0.25, 0)$) -- (gate.input);
					\draw (gate.output) -- ($ (gate.output) + (0.25, 0) $);
				\end{tikzpicture} &
			  \begin{tikzpicture}
				  \node[and gate US, draw] at (0, 0) (gate) {};
				  \draw ($ (gate.input 1) -  (0.25, 0)$) -- (gate.input 1);
				  \draw ($ (gate.input 2) -  (0.25, 0)$) -- (gate.input 2);
				  \draw (gate.output) -- ($ (gate.output) + (0.25, 0) $);
			  \end{tikzpicture} &
			  \begin{tikzpicture}
			  \node[or gate US, draw] at (0, 0) (gate) {};
			  \draw ($ (gate.input 1) -  (0.25, 0)$) -- (gate.input 1);
			  \draw ($ (gate.input 2) -  (0.25, 0)$) -- (gate.input 2);
			  \draw (gate.output) -- ($ (gate.output) + (0.25, 0) $);
			  \end{tikzpicture} &
			  \begin{tikzpicture}
			  \node[xor gate US, draw] at (0, 0) (gate) {};
			  \draw ($ (gate.input 1) -  (0.25, 0)$) -- (gate.input 1);
			  \draw ($ (gate.input 2) -  (0.25, 0)$) -- (gate.input 2);
			  \draw (gate.output) -- ($ (gate.output) + (0.25, 0) $);
			  \end{tikzpicture}
			\\ 
			\multicolumn{2}{c|}{\textbf{Notation}}  & $ \mathtt{\overline{A}} $ & $ \mathtt{A \cdot B} $ & $ \mathtt{A + B} $ & $ \mathtt{A \oplus B} $ \\ 
			\texttt{A} & \texttt{B} & \texttt{not A} & \texttt{A and B} & \texttt{A or B} & \texttt{A xor B} \\ 
			\hline
			$0$ & $0$ & $1$ & $0$ & $0$ & $0$ \\ 
			$0$ & $1$ & $1$ & $0$ & $1$ & $1$ \\ 
			$1$ & $0$ & $0$ & $0$ & $1$ & $1$ \\ 
			$1$ & $1$ & $0$ & $1$ & $1$ & $0$ \\ 
		\end{tabular} 
	\end{table}

	There are multiple standard components which we need to understand for this project.
	\begin{description}[leftmargin=!,labelwidth=\widthof{\bfseries Demultiplexer}]
		\item[Multiplexer] selects between multiple  (1 or more -bit) input lines and outputs it. This can be used for example when \textit{reading} data at a specific location in memory -- the desired address selects the location and outputs the stored data.
		
		\begin{figure}[h]
            \begin{minipage}[b]{.5\linewidth}
                \centering
                \begin{tikzpicture}
                    \node[or gate US, draw, scale=2] at (2, 1) (or) {};
                    \node[and gate US, draw, scale=2] at (0, 2) (and1) {};
                    \node[and gate US, draw, scale=2] at (0, 0) (and2) {};
                    \node [anchor=east] (ctln) at ([xshift=-10]and1.input 1) {$\mathtt{\overline{ctl}}$};
                    \node [anchor=east] (A)    at ([xshift=-10]and1.input 2) {\texttt{A}};
                    \node [anchor=east] (B)    at ([xshift=-10]and2.input 1) {\texttt{B}};
                    \node [anchor=east] (ctl)  at ([xshift=-10]and2.input 2) {\texttt{ctl}};
                    \node [anchor=west] (X)    at ([xshift=10]or.output)     {\texttt{X}};
                    \draw (ctln) -- (and1.input 1);
                    \draw (A)    -- (and1.input 2);
                    \draw (B)    -- (and2.input 1);
                    \draw (ctl)  -- (and2.input 2);
                    \draw (or.output) -- (X);
                    \draw (and1.output) -- ([xshift=10]and1.output) |- (or.input 1);
                    \draw (and2.output) -- ([xshift=10]and2.output) |- (or.input 2);
                \end{tikzpicture}
                \hfill
                \subcaption{2-to-1 multiplexer circuit corresponding to $\mathtt{X = (\overline{ctl} \cdot A)+(ctl \cdot B)}$}\label{fig:21mul}
            \end{minipage}
            \begin{minipage}[b]{.5\linewidth}
                \centering
                \begin{minted}[autogobble, frame=lines, bgcolor=subtlegray]{VHDL}
                    with ctl select X <=
                    	"00" when A,
                    	"01" when B,
                    	"10" when C,
                    	"11" when D;
                \end{minted}
                \subcaption{4-to-1 multiplexer in VHDL, requires 2-bit control signal}\label{fig:41mul}
            \end{minipage}%
            \caption{Multiplexer Circuits}\label{fig:mul}
        \end{figure}
		
		\item[Demultiplexer] is the opposite to a multiplexer -- it distributes input bit/word into one of it's output lines. All the other outputs are $0$. This can be used when \textit{writing} data into a specific memory location where the address serves as the select control.
		\item[Flip flop/latch] is a one bit memory. Flip flop loads in a new value at each clock tick if \texttt{load} control signal is active ($=1$). It can serve as a 1-bit register or for states in a state machine.
		\item[Adders] are used for adding two number.
		\begin{description}
			\item[Half adder] is able to add two bits together and returns the result and carry out bit.
			\item[Full adder] adds three bits (two bits and carry in bit) together. Returns the result and carry out bit. Diagram of its circuit and a truth table describing it's behaviour can be found in figure \ref{fig:fulladd}.
			\begin{figure}[h]
                \begin{minipage}[b]{.5\linewidth}
                    \centering
                    \begin{tikzpicture}
                        \node[and gate US, draw, scale=1.8] at (0, 3) (and1) {};
                        \node[and gate US, draw, scale=1.8] at (0, 2) (and2) {};
                        \node[and gate US, draw, scale=1.8] at (0, 1) (and3) {};
                        \node[xor gate US, draw, scale=2.5] at (2, 0) (xor) {};
                        \node[or gate US,  draw, scale=1.8, logic gate inputs=nnn] at (2, 2) (or) {};
                        
                        \node [anchor=east] (A1)   at ([xshift=-10]and1.input 1) {\texttt{A}};
                        \node [anchor=east] (B1)   at ([xshift=-10]and1.input 2) {\texttt{B}};
                        \node [anchor=east] (A2)   at ([xshift=-10]and2.input 1) {\texttt{A}};
                        \node [anchor=east] (CIN2) at ([xshift=-10]and2.input 2) {\texttt{C\_in}};
                        \node [anchor=east] (B3)   at ([xshift=-10]and3.input 1) {\texttt{B}};
                        \node [anchor=east] (CIN3) at ([xshift=-10]and3.input 2) {\texttt{C\_in}};
                        \node [anchor=east] (AS)   at ([xshift=-10]$(xor.north west)!.5!(xor.input 1)$) {\texttt{A}};
                        \node [anchor=east] (BS)   at ([xshift=-10]xor.west)     {\texttt{B}};
                        \node [anchor=east] (CINS) at ([xshift=-10]$(xor.south west)!.5!(xor.input 2)$) {\texttt{C\_in}};
                        
                        \node [anchor=west] (COUT) at ([xshift=10]or.output)  {\texttt{C\_out}};
                        \node [anchor=west] (SUM)  at ([xshift=10]xor.output) {\texttt{SUM}};
                        
                        \draw (A1)   -- (and1.input 1);
                        \draw (B1)   -- (and1.input 2);
                        \draw (A2)   -- (and2.input 1);
                        \draw (CIN2) -- (and2.input 2);
                        \draw (B3)   -- (and3.input 1);
                        \draw (CIN3) -- (and3.input 2);
                        \draw (AS)   -- ($(xor.north west)!.5!(xor.input 1)$);
                        \draw (BS)   -- (xor.west);
                        \draw (CINS) -- ($(xor.south west)!.5!(xor.input 2)$);
                        
                        \draw (or.output)  -- (COUT);
                        \draw (xor.output) -- (SUM);
                        
                        \draw (and1.output) -- ([xshift=10]and1.output) |- (or.input 1);
                        \draw (and2.output) -- (or.input 2);
                        \draw (and3.output) -- ([xshift=10]and3.output) |- (or.input 3);
                    \end{tikzpicture}
                    \subcaption{Full adder circuit where $\mathtt{SUM = A \oplus B \oplus C\_in}$ and $\mathtt{C\_out = (A \cdot B) + (A \cdot C\_in) + (B \cdot C\_in)}$}\label{fig:fac}
                \end{minipage}
                \begin{minipage}[b]{.5\linewidth}
                    \centering
		            \rowcolors{2}{}{gray!3}
                    \begin{tabular}{c c c|c c}
            			\texttt{A} & \texttt{B} & \texttt{C\_in} & \texttt{SUM} & \texttt{C\_out} \\ \hline
            			0&0&0&0&0 \\
            			0&0&1&1&0 \\
            			0&1&0&1&0 \\
            			0&1&1&0&1 \\
            			1&0&0&1&0 \\
            			1&0&1&0&1 \\
            			1&1&0&0&1 \\
            			1&1&1&1&1 \\
            		\end{tabular}
                    \subcaption{Full adder truth table}\label{fig:fatt}
                \end{minipage}%
                \caption{Full Adder}\label{fig:fulladd}
            \end{figure}
			\item[Ripple adder] uses a chain of full adders to add two multiple-bit numbers.
		\end{description}
	\end{description}
	
	\section{Dissertation Outline}
	The dissertation is structured into the following chapters:
	\begin{description}[leftmargin=!,labelwidth=\widthof{\bfseries Quartus II}]
		\item[\S\ref{ch:Bac} Background] explains necessary concepts, such as Sigma16, M1, Hydra, Hardware Description Languages, FPGA and more.
		\item[\S\ref{ch:Req} Requirements] specify what are the goals of this project. We also discuss blind paths we took in the early stages.
		\item[\S\ref{ch:Des} Design] goes through the details of Sigma16 architecture, the instruction set it supports, the units of M1 circuit and the software and hardware tools we have chosen for this project.
		\item[\S\ref{ch:Imp} Implementation] talks about specific methods, tools, processes we used, how each unit was implemented and some issues we run into.
		\item[\S\ref{ch:Eva} Evaluation] shows the results from each unit, compares our Sigma16 implementation with previous ones, discussed limitation of the system, proposes how to fix these limitations and talks about potential usage of this project.
		\item[\S\ref{ch:Con} Conclusion] summarises up the main achievements, discusses potential future work.
		\item[\S\ref{ap:codebase} -- \S\ref{ap:sims} Appendices] include the structure of the codebase, a tutorial how to run/reproduce this project and more Quartus II RTL views and simulations.
	\end{description}
\end{document}
