\documentclass[../l4proj.tex]{subfiles}

\begin{document}
	\chapter{Design} \label{ch:Des}
	%How is this problem to be approached, without reference to specific implementation details? 
	
	\section{Overview}
	Figure \ref{fig:dsgn} shows the high-level design overview of our system. The computer we are building is deployed on an FPGA board. The main components of the FPGA board are the FPGA chip, Memory, and an Oscillator. The FPGA chip reads/writes from/to the Memory and the computer's internal clock is driven by an oscillator. The FPGA (chip) should be configured as circuits of the three main units -- Datapath, ALU and Control Unit. The Control Unit generates control signals based on the current instruction and these signals are then used to control components in the Datapath. The ALU performs arithmetic and logic operations and sends the results back to the Datapath.
	
    \begin{figure}[h]
        \centering
        \includegraphics[width=\textwidth]{images/DesignOverview.png}
        \caption{Design Overview}
        \label{fig:dsgn}
    \end{figure}	
	
	\section{Sigma16 Architecture}\label{sec:S16Arch}
	Sigma16 is a 16-bit computer architecture. It uses 16-bit words and a register file with 16 16-bit registers. A slightly simplified architecture diagram can be found on Figure \ref{fig:Arch}.
	The architecture contains these registers:
	\begin{description}
		\item[ir] instruction register, holds the instruction currently being executed;
		\item[pc] program counter, holds the address of the next instruction to be executed;
		\item[ad] address register, holds the second word of \texttt{RX} instructions and is used for calculating effective address;
		\item[register file] contains 16 16-bit registers
	\end{description}
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=\textwidth]{images/Sigma16Architecture.jpg}
		\caption{Sigma16 Architecture (simplified) diagram} \label{fig:Arch}
		\captionsetup{font={footnotesize,it}}
		\caption*{Source: \cite{CS2Lectures} -- Lecture 5: From the RTM to the Sigma16}
	\end{figure}
	
	
	\subsection{Sigma16 Instruction set} \label{S16ISA}
	Sigma16 has its own instruction set. Each instruction can take one of three formats -- \texttt{RRR}, \texttt{RX} or \texttt{EXP}.
	\begin{description}
		\item[\texttt{RRR}] takes one word - 16 bits. First four bits represent the operation code (\texttt{op}), next four bits is the index of the destination register (\texttt{d}), next four bits is the index of source a register (\texttt{sa}) and the last four bits are the index of the source b register (\texttt{sb}). For example ``\texttt{0x0123}'' can be read as \texttt{add R1,R2,R3} which takes the values in R2 and R3, adds them together and stores the result in R1. The zero in the beginning of the instruction in this case is the op code for addition. See list of all \texttt{RRR} instructions in Table \ref{tab:RRR}.
		\item[\texttt{RX}] consists of two words. The first word consist of 4-bit op code (needs to be of value \texttt{f} in hexadecimal), last four bits specify which specific \texttt{RX} instruction it is, second four bits are the destination register and the following four bits are the index of the register containing offset value. The second word contains the 16-bit address. For example ``0xf121 0x0501'' is \texttt{load R1, 0501[R2]} and that is loading value into \texttt{R1} from memory location at \texttt{0501} plus the value in the offset register \texttt{R2}. All \texttt{RX} instructions can be found in Table \ref{tab:RX}.
		\item[\texttt{EXP}] stands for both EXPerimental and EXPansion. There is space for 256 extra \texttt{EXP} instructions, but we won't be using them at all in our design.
	\end{description}
	
	\begin{table}[h]
		\caption{RRR instructions}\label{tab:RRR}
		\rowcolors{2}{}{gray!3}
		\begin{tabular}{rllll}
			\textbf{op} & \textbf{format} & \textbf{mnemonic} & \textbf{operands (d, a, b)} & \textbf{action} \\ 
			$0$ & \texttt{RRR} & add & \texttt{R1, R2, R3} & \texttt{R1 := R2+R3} \\ 
			$1$ & \texttt{RRR} & sub & \texttt{R1, R2, R3} & \texttt{R1 := R2-R3} \\ 
			$2$ & \texttt{RRR} & mul & \texttt{R1, R2, R3} & \texttt{R1 := R2*R3, R15 := high word} \\ 
			$3$ & \texttt{RRR} & div & \texttt{R1, R2, R3} & \texttt{R1 := R2/R3, R15 := R2 mod R3} \\ 
			$4$ & \texttt{RRR} & cmplt & \texttt{R1, R2, R3} & \texttt{R1 := R2<R3} \\ 
			$5$ & \texttt{RRR} & cmpeq & \texttt{R1, R2, R3} & \texttt{R1 := R2=R3} \\ 
			$6$ & \texttt{RRR} & cmpgt & \texttt{R1, R2, R3} & \texttt{R1 := R2>R3} \\ 
			$7$ & \texttt{RRR} & inv & \texttt{R1, R2, R3} & \texttt{R1 := bwinv R2} \\ 
			$8$ & \texttt{RRR} & and & \texttt{R1, R2, R3} & \texttt{R1 := R2 bwand R3} \\ 
			$9$ & \texttt{RRR} & or & \texttt{R1, R2, R3} & \texttt{R1 := R2 bwor R3} \\ 
			$a$ & \texttt{RRR} & xor & \texttt{R1, R2, R3} & \texttt{R1 := R2 bwxor R3} \\ 
			$b$ & \texttt{RRR} & shiftl & \texttt{R1, R2, R3} & \texttt{R1 := R2 shiftl R3 bits} \\ 
			$c$ & \texttt{RRR} & shiftr & \texttt{R1, R2, R3} & \texttt{R1 := R2 shiftr R3 bits} \\ 
			$d$ & \texttt{RRR} & trap & \texttt{R1, R2, R3} & \texttt{trap interrupt} \\ 
			$e$ & \texttt{EXP} &  &  & expand to EXP format \\ 
			$f$ & \texttt{RX} &  &  & expand to RX format \\ 
		\end{tabular} 
	\end{table}

	\begin{table}[h]
		\caption{RX instructions}\label{tab:RX}
		\rowcolors{2}{}{gray!3}
		\begin{tabular}{rrllll}
			\textbf{op} & \textbf{b} & \textbf{format} & \textbf{mnemonic} & \textbf{operands} & \textbf{action} \\ 
			\texttt{f} & 0 & \texttt{RX} & lea & \texttt{Rd, x[Ra]} & \texttt{Rd := x+Ra} \\ 
			\texttt{f} & 1 & \texttt{RX} & load & \texttt{Rd, x[Ra]} & \texttt{Rd := mem[x+Ra]} \\ 
			\texttt{f} & 2 & \texttt{RX} & store & \texttt{Rd, x[Ra]} & \texttt{mem[x+Ra] := Rd} \\ 
			\texttt{f} & 3 & \texttt{RX} & jump & \texttt{Rd, x[Ra]} & \texttt{pc := x+Ra} \\ 
			\texttt{f} & 4 & \texttt{RX} & jumpf & \texttt{Rd, x[Ra]} & \texttt{if Rd==0 then pc := x+Ra} \\ 
			\texttt{f} & 5 & \texttt{RX} & jumpt & \texttt{Rd, x[Ra]} & \texttt{if Rd/=0 then pc := x+Ra} \\ 
			\texttt{f} & 6 & \texttt{RX} & jal & \texttt{Rd, x[Ra]} & \texttt{Rd := pc, pc := x+Ra} \\ 
		\end{tabular} 
	\end{table}

	\pagebreak
	\subsection{M1}\label{sec:M1}
	M1 is a digital circuit that implements the core subset of Sigma16 architecture. It does not handle overflow, interrupts and I/O (mostly EXP instructions). Also some RRR and RX instructions are not fully implemented -- \texttt{mul}, \texttt{div}, the bitwise logic and shift instructions.
	
	M1 is built entirely from logic gates, flip flops and wires. There are other implementations of Sigma16, but they only emulate the behaviour. Hence we will be using M1 circuit as the model for our implementation.
	
	M1 consists of three main units: datapath, control algorithm and memory. The datapath also contains ALU (Arithmetic \& Logic Unit). Let's discuss more in detail what each unit does.

	\subsubsection{Datapath}
	Datapath describes how the data flows in the system. It contains all registers -- register file, program counter, instruction register, address register. There are by default 16 general purpose registers in the register file, although their number can be parametrised.
	
	Datapath also contains combinational (time-independent) logic. Those are mostly multiplexers deciding which data bus should be connected to some component inputs. For example, the first word input to the ALU can be either the program counter or the first word output from the register file.
	
	\pagebreak
	\subsubsection{ALU}
	The Arithmetic Logic Unit has the following interface:
	\begin{itemize}
		\item inputs:
		\begin{itemize}
			\item word size,
			\item four control signals determining the required operation to be performed,
			\item two words of the given size to perform the operation on;
		\end{itemize}
		\item outputs:
		\begin{itemize}
			\item result word from the operation of the given size,
			\item overflow bit (if relevant).
		\end{itemize}%
	\end{itemize}%
	Operations supported by the ALU can be seen in Table \ref{tab:ALU_op} along with their corresponding control signals.
	\begin{table}[h]
	    \caption{ALU operations determined by 4 control signals, `\texttt{x}' stands for ``don't care'' value.}\label{tab:ALU_op}
	    \rowcolors{2}{}{gray!3}
	    \begin{tabular}{cc}
            \textbf{\texttt{abcd}} & \textbf{operation} \\
	        \texttt{00xx} & \texttt{x+y} \\
	        \texttt{01xx} & \texttt{x-y} \\
	        \texttt{10xx} & \texttt{-x} \\
	        \texttt{1100} & \texttt{x+1} \\
	        \texttt{1101} & \texttt{x<y} \\
	        \texttt{1110} & \texttt{x=y} \\
	        \texttt{1111} & \texttt{x>y} \\
	    \end{tabular}
	\end{table}
	
	\subsubsection{Control Algorithm}
	Control algorithm generates control signals which then control the combinational logic in the datapath. The signals are generated using a state machine which is timed. At every clock tick, the current state changes.
	
	For example \texttt{ir=0x1123} corresponds to the instruction \texttt{sub R1,R2,R3} -- subtract the value stored in register 3 from the value stored in register 2 and put the result into register 1. When performing this instruciton, the state machine would go into state \texttt{ctl\_sub}\footnote{A list of all control states and signal can be found in appendix \ref{ap:ctl}.} which would ensure that \texttt{ctl\_alu\_(abcd) = 0b0100} (for the ALU to perform subtraction), and also turn on both \texttt{ctl\_rf\_alu} and \texttt{ctl\_rf\_ld} so that the result from ALU is loaded into the register file. Then the state machine goes into the next state which turns on necessary signals to load the next instruction into \texttt{ir}. And so on.
	
	\subsubsection{Memory}
	The memory stores the program instructions as well as the execution stack. It contains $ 2^{16} $ 16-bit words.
	
	The M1 model of Sigma16 assumes any memory operation takes only 1 clock cycle for simplicity. Since communication with memory on a real hardware is usually one of the slowest operations, it means either the clock cycle needs to be as slow as the memory operation or the control algorithm needs to wait more clock cycles until the memory operation has finished.
	
	\pagebreak
	\section{Hardware and Software Tools}
    We need to choose proper tools for the development of our device. We need hardware to deploy the circuit on and also a suitable synthesis tool for describing, simulating and synthesising the circuit.
    
	\subsection[Altera DE1 Cyclone II FPGA Board]{Altera DE1 Cyclone II FPGA Board\footnote{\url{https://www.intel.com/content/www/us/en/programmable/solutions/partners/partner-profile/terasic-inc-/board/altera-de1-board.html}}} \label{sec:DE1CII}
	FPGA development boards contain not only an FPGA (the programmable chip), but also other components such as memory, LEDs, switches, VGA output, etc. It is possible to connect these to an FPGA itself, but it takes complex soldering. That is useful when creating a device to be manufactured in bigger numbers since including only needed features makes it cheaper. However the extra effort put into soldering would add additional level to the complexity of this project. Therefore we are looking for an FPGA development board which makes this project reproducible as easily as possible.
	
	The FPGA board features we are looking for are:
	\begin{itemize}
		\item $ 128 \mathtt{KB} $ of memory, that is $ 2^{16} $ addresses containing 16-bit words;
		\item USB-Blaster for communication over USB with a PC;
		\item clock for synchronous parts of our circuit, no specific frequency required;
		\item some built-in LEDs and switches for some basic debugging, but this is not a necessity;
		\item the board should be supported by a free (student) version of a synthesis tool. Licences for the professional versions are usually in range of three to four digit figures.
	\end{itemize}
	These requirements (except for the last one) are satisfied by majority of the modern FPGA boards.
	
	The University of Glasgow School of Engineering lent us Altera DE1 Cyclone II FPGA Board for this project. However, any other modern FPGA board with the required features could be substituted with only a little extra work - different board would need different pin mapping, and memory operations and PC communication would need to be reviewed. The rest of the project such as the VHDL code (except for memory and PC communication) would remain unchanged.
	
	The FPGA board (see figure \ref{fig:CIIFPGA}) was manufactured in year 2006 and there were many subversions of it with minor differences which are not well documented online. The board itself came with 2 CD disks containing documentation and tutorials and an installation disk for a synthesis tool. The CDs, however, were for a  different version of the board (DE2). We managed to find the files for the same version (DE1) online\footnote{\url{http://download.terasic.com/downloads/cd-rom/de1/}}, however it was not completely the same model. Some of the tutorials were for the DE2 board and had datasheets for different memory chips. For example our board has \texttt{A2V64S40CTP} SDRAM while the datasheet provided was for \texttt{IS42S16400} SDRAM. This created difficulties discussed later in section \ref{sec:Mem}.
		
	\begin{figure}[h]
		\centering
		\includegraphics[width=\textwidth]{images/CycloneIIFPGA.jpg}
		\captionsetup{font={footnotesize,it}}
		\caption*{Source: \cite{CIIFPGAman}}
		\captionsetup{font={}}
		\caption{Cyclone II FPGA Starter Board} \label{fig:CIIFPGA}
	\end{figure}
	
	\subsection{Quartus II}\label{QuartusII}
	Synthesis tools can be perceived as an IDE for HDLs (such as e.g. PyCharm, VSCode for programming languages). I.e. it usually has a code editor with syntax highlighting and checking. Additionally, synthesis tool is also responsible for synthesising the described circuit which can be compared to interpreting/compiling the language. The synthesis is dependant on the hardware we are using as well as the synthesis tool as each of those tools performs different optimizations. However, the resulting circuit always behaves functionally in the same way as the original circuit described by the programmer.
	
	The only compatible synthesis tool for the Altera DE1 FPGA board is Quartus II by Altera. We are using Quartus II version 13.0 which is the latest version supporting the FPGA on our board (\texttt{EPC2C20F484C7N}).
	
	Quartus II has a free student version, hence this combination of the FPGA board with this synthesis tool is optimal. The free version does not include all Quartus II features, but it does include all the features we need: a VHDL editor and compiler, functional and timing simulator (see section \ref{sec:sims}), pin assignment (for connecting the boards' inputs/outputs with the correct components such as a memory).
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{images/PinPlanner.jpg}
		\caption{Pin Assignment in Quartus II. This is an example of connecting a Flash memory with the FPGA. The memory connections are soldered to specific pins on the FPGA, such as \texttt{PIN\_AB20}. Hence we need to specify  which pins should the circuit inputs/outputs be connected with internally on the FPGA.}\label{fig:Pin}
	\end{figure}
	
\end{document}
