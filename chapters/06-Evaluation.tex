\documentclass[../l4proj.tex]{subfiles}

\begin{document}
	\chapter{Evaluation}\label{ch:Eva}
	
	%How good is your solution? How well did you solve the general problem, and what evidence do you have to support that?
	
	% Simulations
	% FPGA demo?
%	qualitative quantitative
	\section{Results}\label{sec:res}
    We successfully demonstrated that each part of the system works separately. Datapath, ALU and Control Unit were simulated using Quartus II simulator. Clock, Memory and PC communication were tested directly on the FPGA board.
	
	\subsection{Simulations}\label{sec:sims}
	For simulating the circuit we used Quartus II simulator. It can run Functional or Timing Simulation. Functional Simulation shows how output and internal signal values change over time based on given input signals. It ignores any gate delay, so it only shows if the logic of the circuit is correct. The Timing Simulation also takes into account the gate delay. The Functional Simulation is similar to Hydra simulations in terms of how well does it simulate the real circuit behaviour.
	
	\subsubsection{ALU Simulations\\}
	We set the control input signals to test all the operations listed in table \ref{tab:ALU_op}. Timing Simulation can be seen in figure \ref{fig:ALUsim}. There is a row per input/output and time continually changes from left to right. For single bit signals, the line represents the value, i.e. low for \texttt{0}, high for \texttt{1} or crossed out for unknown value. For words, the value is written there in signed decimal radix. It is a timing simulation so it shows how the circuit behaves realistically by including gate delays. This can be observed in the figure on the output \texttt{Z} whenever the control inputs change. For example the output is stabilises around the pointer at \SI{165}{\nano\second} while the control signals have already changed at \SI{150}{\nano\second}, so the circuit has almost \SI{20}{\nano\second} delay. This had to be taken into account when we were deciding how fast can we drive the system. However, memory access causes more significant delays and so we have to wait for the slowest part of the system.
	
	As for the functional part of the simulation, following the table \ref{tab:ALU_op} we can confirm the output values in figure \ref{fig:ALUsim} are as expected. We set the input words to be $42$ and $23$. In the first interval where controls are set to perform addition, the output word is indeed $42+23=65$. Then followed by subtraction, negation, incrementing, and comparisons. We have also tested different input values to test overflow behaviour and other edge conditions, we do not show them here for simplicity.
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{images/ALUsim.jpg}
		\caption{Timing Simulation of ALU in Quartus II}\label{fig:ALUsim}
	\end{figure}
	
	Interesting thing to note is the overflow (\texttt{C\_OUT}) output is $1$ when subtracting $42-23=19$. This is due to the fact the subtraction actually does addition of the first operand with the two's complement of the second operand. That is done by adding first operand with inverted second operand using the same ripple adder as for addition, but setting carry in to $1$ on the least significant place. Assuming 7-bit inputs, in binary this is
	\begin{align*}
	    0101010 - 0010111 &= 0101010 + 1101000 + 1\\
	                      &= \textbf{1}|0010011\\
	                      &= 19\text{ dec (ovfl 1)}
	\end{align*}
    
    \subsubsection{Control Unit Simulations\\}
    The Control Unit circuit has significantly more inputs/outputs than ALU so here we will show only a part of the simulation going through \texttt{add} and \texttt{load} instructions. Functional Simulation can be seen in figure \ref{fig:CtlSim}. We have omitted signals which were $0$ all the time.
    
    Our simplified test program has the following instructions:
    \begin{minted}[autogobble, frame=lines, bgcolor=subtlegray, escapeinside=||]{asm}
        add  R1,R2,R3 ; R1 = R2+R3
        load R1,x[R0] ; R1 = x[R0] where x is at address ADDE
    \end{minted}
    Since we are simulating only the Control circuit and not the whole circuit we need to supply expected inputs manually. We start with \texttt{RESET} on and once we turn it off, the state machine goes from \texttt{st\_instr\_fet} to \texttt{st\_dispatch} and the first instruction (\texttt{add R1,R2,R3 \textrightarrow\ 0x0123}) is loaded by us, mocking the expected behaviour of Datapath. Then control recognises the instruction and goes to the \texttt{st\_add} state. Afterwards it goes back to \texttt{st\_instr\_fet} and \texttt{st\_dispatch} where we supply the next instruction (\texttt{load R1,x[R0] \textrightarrow~0xF101}). The \texttt{load} instruction execution consists of three states. The first one sets the control signals to load another word from the memory to \texttt{IR} which is the address of the variable \texttt{x} (in this example this is \texttt{0xADDE}). After the three \texttt{load} states, the machine goes back to \texttt{st\_instr\_fet}. And that is where we end this example after demonstrating both \texttt{RRR} and \texttt{RX} instructions.
    
    \begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{images/ControlSim.jpg}
		\caption{Functional Simulation of Control Circuit in Quartus II}\label{fig:CtlSim}
	\end{figure}
	
	Interesting thing to note is there is a difference between the Timing Simulation and the Functional Simulation if the \texttt{RESET} signal turns off right on a rising edge of the clock (from $0$ to $1$, in figure \ref{fig:CtlSim} it happened on the falling edge -- from $0$ to $1$). If that happens, the \texttt{START} output takes some time to change and since \texttt{st\_instr\_fet} is always the next state when \texttt{START} is 1, the state machine goes into \texttt{st\_dispatch} one cycle later as it did not recognise the change in \texttt{START} in the previous one. Hence the timing Simulation reveals extra one cycle long delay.
	
	\subsection{Deployment on FPGA} \label{sec:Deploy}
	We have tested the Clock and the SDRAM reading and writing directly on the FPGA board.
	
	A clock signal is generated by a \SI{24}{\mega\hertz} oscillator, we slowed the clock down to \SI{1}{\hertz} and connected the result to left-most red LED on the board (\texttt{LEDR9}).
	
	Every second we also wrote a new value into a new address in the SDRAM and confirmed the value was stored by reading it and showing it on the 16 right-most LEDs (8 green and 8 red).
	
	A photo of the demonstration is in figure \ref{fig:sdramclk} (unfortunately we cannot include a video of it changing every second).
	
	\begin{figure}[h]
	    \centering
	    \includegraphics[width=\textwidth]{images/FPGA_mem.jpg}
	    \caption{Demonstration of SDRAM and Clock operations on the FPGA board.}
	    \label{fig:sdramclk}
	\end{figure}
	
	\section{Limitations}\label{sec:Limitations}
	\subsection{PC Communication with Memory}\label{sec:memIss}
	We have successfully tested each component of the whole system separately. However, we ran into some issues when putting the system together. 
	As discussed in section \ref{sec:Mem}, we were not able to put PC communication together with Memory. To summarise the issues:
    \begin{itemize}
        \item We successfully demonstrated using SDRAM, but we cannot use it together with the PC communication because PC communication restarts the memory.
        \item We could use Flash Memory instead which is non-volatile, but we were unable to use the specific Flash Memory on the FPGA board because of the lack of documentation.
    \end{itemize}
    There are (at least) three ways to fix these issues in the future.
    \begin{enumerate}
        \item Find a way to use the Control Panel without restarting the board which would mean we can freely use the SDRAM. This would require familiarising with Verilog as the only source code we found for the Control Panel is written in it, modifying this code to incorporate it into our own system and finally, load the joined system on the board.
        \item Define our own USB interface both on the PC and the FPGA. This would require familiarising with the necessary protocols, possibly by reading the Control Panel's Verilog code as we were unable to find other online resource specifically for our hardware.
        \item Debug why the Flash Memory does not work. This could be for reasons mentioned in section \ref{sec:flash}, but also for other reason we did not think of.
    \end{enumerate}
    All of these fixes require significant time we did not have, so unfortunately we had to leave this unfinished for now.

	\subsection{Hydra vs VHDL implementation of Sigma16 Architecture}\label{sec:HvVHDL}
	Since we did not manage to put the whole system together in VHDL, it is difficult to compare the two systems. Each VHDL component was tested its every operation works as expected, but we cannot guarantee with certainty that the VHDL system, once finished, would yield the same results as the Hydra system. In theory both the implementations support the same set of instructions so both systems should be capable of executing the same programs and returning the same results.
	
	Both implementations do not support any \texttt{EXP} instructions from the Sigma16 architecture. So neither can handle any user input or pipelining.
	
	And of course, neither of the systems are even close to modern computers in terms of performance. Our system can be run at most at \SI{1}{\mega\hertz} while modern processors operate at a few \SI{}{\giga\hertz}. But as mentioned earlier, the point of the architecture is not to be high-performing, but to give insight into computer operation without too many details.
	
	\section{Potential Use as a Coursework}\label{sec:Coursework}
	The VHDL implementation of Sigma16 could be used in the same manner as the Hydra implementation for teaching purposes.
	
	This design could be extended by adding support for some unimplemented instructions, such as multiplication. Similar task is already assigned as a coursework in the honours Computer Architecture course at University of Glasgow, only difference is the task is for M1 in Hydra.
	
	Based on the lecturer's response to student feedback \citep{CAfeedback}, students would prefer using industrial languages and architectures. The point of using Sigma16 and Hydra is to focus on the core principles rather then specific language or very complex architectures. Using VHDL implementation of Sigma16 would be a great compromise -- simple architecture with an industrial language. This approach would also be more ``hands on''.
	
	The coursework could consist of two tasks. First simpler task would be to implement the multiplication instruction. That requires adding multiplication unit and a few changes to the control algorithm. Second, advanced task would be adding support for external interrupts following the newer versions of Sigma16 architecture.
\end{document}
